<?php
/**
 * Created by PhpStorm.
 * User: q
 * Date: 12/1/18
 * Time: 8:39 PM
 */

namespace MiamiOH\WSEmployeeClassRule\EloquentModels;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Yajra\Oci8\Query\OracleBuilder;

class EmployeeClassRule extends Model
{
    /**
     * @var string $connection Connection
     */
    protected $connection = 'MUWS_GEN_PROD';
    /**
     * @var string $table Table name
     */
    public $table = 'ptrecls';

    /**
     * @var bool $timestamps Do not populate auto-generated date fields
     */
    public $timestamps = false;

    /**
     * @var bool $incrementing Do not increment primary key by default
     */
    public $incrementing = false;

    /**
     * @var string $primaryKey Primary key of table
     */
    protected $primaryKey = 'ptrecls_code';

    /**
     * @var array $guarded black list of insertable fields
     */
    protected $guarded = [];

    /**
     * Get a new query builder instance for the connection.
     * https://github.com/yajra/laravel-oci8/issues/73#issuecomment-117131744
     *
     * @return \Illuminate\Database\Query\Builder
     */
    protected function newBaseQueryBuilder()
    {
        $conn = $this->getConnection();

        $grammar = $conn->getQueryGrammar();

        return new OracleBuilder($conn, $grammar, $conn->getPostProcessor());
    }

    public function scopeBuildSelect(Builder $query)
    {
        return $query->select(
            [
                'ptrecls_code',
                'ptrecls_short_desc',
                'ptrecls_long_desc',
                'ptrecls_bcat_code',
                'ptrecls_egrp_code',
                'ptrecls_empr_code',
                'ptrecls_flsa_ind',
                'ptrecls_lcat_code',
                'ptrecls_pcat_code',
                'ptrecls_pict_code',
            ]
        );
    }

    public function scopeWhereCodes(Builder $query, array $codes)
    {
        if (!empty($codes)) {
            return $query->whereIn('ptrecls_code', $codes);
        }
    }

    public function scopeWhereTypeahead(Builder $query, string $code)
    {
        $code = strtoupper($code);

        if (!empty($code)) {
            return $query
                ->whereRaw('UPPER("PTRECLS_LONG_DESC") LIKE ?',"%$code%")
                ->orWhereRaw('UPPER("PTRECLS_CODE") LIKE ?',"%$code%");
        }
    }
}
