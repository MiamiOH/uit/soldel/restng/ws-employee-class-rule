<?php

namespace MiamiOH\WSEmployeeClassRule\Repositories;

use MiamiOH\WSEmployeeClassRule\DataTransformers\DataTransformer;
use MiamiOH\WSEmployeeClassRule\EloquentModels\EmployeeClassRule;

/**
 * Class EmployeeClassRuleRepositorySQL
 */
class EmployeeClassRuleRepositorySQL
{
    private $dataTransformer;

    /**
     * EmployeeClassRuleRepositorySQL constructor.
     * @param DataTransformer $dataTransformer
     */
    public function __construct(DataTransformer $dataTransformer)
    {
        $this->dataTransformer = $dataTransformer;
    }

    /**
     * @param array $employeeClassCode
     * @return array
     */
    public function getEmployeeClassRule(
        array $employeeClassCode
    ): array
    {
        $query = EmployeeClassRule::buildSelect();

        $query->whereCodes($employeeClassCode);

        $records = $query->get();

        return $this->dataTransformer->transformData($records);
    }

    /**
     * @param string $code
     * @return array
     */
    public function getEmployeeClassRuleTypeahead(
        string $code
    ): array
    {
        $query = EmployeeClassRule::buildSelect();

        $query->whereTypeahead($code);

        $records = $query->get();

        return $this->dataTransformer->transformData($records);
    }


}
