<?php
/**
 * Created by PhpStorm.
 * User: q
 * Date: 2/5/19
 * Time: 4:50 PM
 */

namespace MiamiOH\WSEmployeeClassRule\DataTransformers;


use Illuminate\Support\Collection;

class EmployeeClassRuleDataTransformer extends DataTransformer
{
    private $keyExchanges = [
        'employeeClassCode' => 'ptrecls_code',
        'shortDescription' => 'ptrecls_short_desc',
        'longDescription' => 'ptrecls_long_desc',
        'benefitCategoryCode' => 'ptrecls_bcat_code',
        'employeeGroupCode' => 'ptrecls_egrp_code',
        'employerIdentificationCode' => 'ptrecls_empr_code',
        'flsaIndicator' => 'ptrecls_flsa_ind',
        'leaveCategoryCode' => 'ptrecls_lcat_code',
        'premiumPayCategoryCode' => 'ptrecls_pcat_code',
        'payrollIdentificationCode' => 'ptrecls_pict_code',
    ];

    /**
     * @param Collection $collection
     * @param array $options
     * @return array
     */
    public function transformData(Collection $collection, array $options = [])
    {
        $result = [];

        foreach($collection as $index => $model) {
            $modelAttributes = $model->getAttributes();

            foreach($this->keyExchanges as $key => $val) {
                $result[$index][$key] = $modelAttributes[$val];
            }

            $result[$index]['combinedDescription'] = $result[$index]['longDescription'] . ' (' . $result[$index]['employeeClassCode'] . ')';
        }

        return $result;
    }
}