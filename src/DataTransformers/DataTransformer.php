<?php
/**
 * Created by PhpStorm.
 * User: q
 * Date: 2/5/19
 * Time: 4:43 PM
 */

namespace MiamiOH\WSEmployeeClassRule\DataTransformers;

use Illuminate\Support\Collection;

abstract class DataTransformer
{
    abstract public function transformData(Collection $collection, array $options = []);
}