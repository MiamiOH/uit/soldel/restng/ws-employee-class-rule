<?php

namespace MiamiOH\WSEmployeeClassRule\Resources;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\ResourceProvider;
use MiamiOH\WSEmployeeClassRule\Services\EmployeeClassRule\Service;

class EmployeeClassRuleResourceProvider extends ResourceProvider
{
    private $name = 'EmployeeClassRule';

    public function registerDefinitions(): void
    {
        $this->addDefinition([
            'name' => $this->name . '.model',
            'type' => 'object',
            'properties' => [
                'employeeClassCode' => [
                    'type' => 'string',
                ],
                'employeeGroupCode' => [
                    'type' => 'string',
                ],
                'shortDescription' => [
                    'type' => 'string',
                ],
                'longDescription' => [
                    'type' => 'string',
                ],
                'payrollIdentificationCode' => [
                    'type' => 'string',
                ],
                'hoursPerPay' => [
                    'type' => 'string',
                ],
                'hoursPerDay' => [
                    'type' => 'string',
                ],
                'shiftCode' => [
                    'type' => 'string',
                ],
                'salaryEncumbranceIndicator' => [
                    'type' => 'string',
                ],
                'timesheetRosterIndicator' => [
                    'type' => 'string',
                ],
                'defaultHoursIndicator' => [
                    'type' => 'string',
                ],
                'salaryIndicator' => [
                    'type' => 'string',
                ],
                'budgetRollIndicator' => [
                    'type' => 'string',
                ],
                'leaveCategoryCode' => [
                    'type' => 'string',
                ],
                'benefitCategoryCode' => [
                    'type' => 'string',
                ],
                'eeoContractGroupCode' => [
                    'type' => 'string',
                ],
                'employerIdentificationCode' => [
                    'type' => 'string',
                ],
                'workPeriodCode' => [
                    'type' => 'string',
                ],
                'flsaIndicator' => [
                    'type' => 'string',
                ],
                'timeEntryIndicator' => [
                    'type' => 'string',
                ],
                'timeEntryDay' => [
                    'type' => 'string',
                ],
                'deferPayPeriodRuleCode' => [
                    'type' => 'string',
                ],
                'premiumRollIndicator' => [
                    'type' => 'string',
                ],
                'internalFullTimePartTimeIndicator' => [
                    'type' => 'string',
                ],
                'timeEntryMethod' => [
                    'type' => 'string',
                ],
                'LeaveReportingMethod' => [
                    'type' => 'string',
                ],
                'leaveReportPayrollIdentificationCode' => [
                    'type' => 'string',
                ]
            ]
        ]);

        $this->addDefinition([
            'name' => $this->name . '.collection',
            'type' => 'array',
            'items' => [
                '$ref' => '#/definitions/' . $this->name . '.model'
            ]
        ]);

    }

    public function registerServices(): void
    {
        $this->addService([
            'name' => $this->name,
            'class' => Service::class,
            'description' => 'Provide database handler'
        ]);
    }

    public function registerResources(): void
    {
        $this->addResource([
            'action' => 'read',
            'description' => 'Read Employee Class Rule table',
            'name' => $this->name . '.get',
            'service' => $this->name,
            'method' => 'getEmployeeClassRule',
            'pattern' => '/employeeClassRule/v1',
            'options' => [
                'employeeClassCode' => [
                    'type' => 'list',
                    'description' => 'list of employee class code'
                ],
            ],
            'responses' => [
                App::API_OK => [
                    'description' => 'A collection of ID records.',
                    'returns' => [
                        'type' => 'array',
                        '$ref' => '#/definitions/' . $this->name . '.collection',
                    ]
                ],
                App::API_NOTFOUND => [
                    'description' => 'There is no record found.',
                ],
                App::API_UNAUTHORIZED => [
                    'description' => 'Unauthorized access.',
                ],
            ]
        ]);

        $this->addResource([
            'action' => 'read',
            'description' => 'Read Employee Class Rule table typeahead',
            'name' => $this->name . '.get.typeahead',
            'service' => $this->name,
            'method' => 'getEmployeeClassRuleTypeAhead',
            'pattern' => '/employeeClassRule/typeahead/v1',
            'options' => [
                'q' => [
                    'type' => 'single',
                    'required' => true,
                    'description' => 'employee class code query'
                ],
            ],
            'responses' => [
                App::API_OK => [
                    'description' => 'A collection of ID records.',
                    'returns' => [
                        'type' => 'array',
                        '$ref' => '#/definitions/' . $this->name . '.collection',
                    ]
                ],
                App::API_NOTFOUND => [
                    'description' => 'There is no record found.',
                ],
                App::API_UNAUTHORIZED => [
                    'description' => 'Unauthorized access.',
                ],
            ]
        ]);
    }

    public function registerOrmConnections(): void
    {

    }
}
