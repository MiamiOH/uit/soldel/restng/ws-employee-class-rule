<?php
/**
 * Created by PhpStorm.
 * User: q
 * Date: 9/26/18
 * Time: 10:39 AM
 */

namespace MiamiOH\WSEmployeeClassRule\Services\EmployeeClassRule;

use MiamiOH\RESTng\Util\Response;
use MiamiOH\RESTng\Util\Request;
use MiamiOH\WSEmployeeClassRule\Repositories\EmployeeClassRuleRepositorySQL;
use \MiamiOH\RESTngIlluminateIntegration\RESTngValidatorFactory;

class Get
{
    private $rules = [
        'code' => ['min:1', 'max:2']
    ];

    /**
     * @param Request $request
     * @param Response $response
     * @param EmployeeClassRuleRepositorySQL $repository
     * @return Response
     * @throws \Exception
     */
    public function getEmployeeClassRules(
        Request $request,
        Response $response,
        EmployeeClassRuleRepositorySQL $repository
    ): Response
    {
        $status = \MiamiOH\RESTng\App::API_OK;

        $options = $request->getOptions();

        $codes = $options['employeeClassCode'] ?? [];

        $validCodes = [];

        // Validate code
        foreach ($codes as $code) {
            $validator = RESTngValidatorFactory::make(['code' => $code], $this->rules);

            if ($validator->passes()) {
                $validCodes[] = $code;
            }
        }

       $records = $repository->getEmployeeClassRule(
            $validCodes
        );

        // DONE
        $response->setPayLoad($records);
        $response->setStatus($status);
        return $response;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param EmployeeClassRuleRepositorySQL $repository
     * @return Response
     * @throws \Exception
     */
    public function getEmployeeClassRulesTypeahead(
        Request $request,
        Response $response,
        EmployeeClassRuleRepositorySQL $repository
    ): Response
    {
        $status = \MiamiOH\RESTng\App::API_OK;

        $options = $request->getOptions();

        $code = $options['q'] ?? '';

        $code = trim($code);

        $validator = RESTngValidatorFactory::make(['code' => $code], $this->rules);

        if ($validator->fails()) {
            $response->setPayLoad(['errors' => $validator->errors()->getMessages()]);
            $response->setStatus(\MiamiOH\RESTng\App::API_BADREQUEST);
        }

        $records = $repository->getEmployeeClassRuleTypeahead(
            $code
        );

        // DONE
        $response->setPayLoad($records);
        $response->setStatus($status);
        return $response;
    }
}
