<?php
/**
 * Created by PhpStorm.
 * User: q
 * Date: 9/24/18
 * Time: 3:05 PM
 */

namespace MiamiOH\WSEmployeeClassRule\Services\EmployeeClassRule;

use MiamiOH\RESTngIlluminateIntegration\RESTngEloquentFactory;
use MiamiOH\RESTng\Util\Request;
use MiamiOH\RESTng\Util\Response;
use MiamiOH\RESTng\Util\User;
use MiamiOH\RESTng\Legacy\DataSource;
use MiamiOH\WSEmployeeClassRule\DataTransformers\EmployeeClassRuleDataTransformer;
use MiamiOH\WSEmployeeClassRule\Repositories\EmployeeClassRuleRepositorySQL;

class Service extends \MiamiOH\RESTng\Service
{
    /**
     * @var Request
     */
    private $request = null;

    /**
     * @var Response
     */
    private $response = null;

    /**
     * @var User
     */
    protected $user = null;

    /**
     * @var EmployeeClassRuleRepositorySQL
     */
    protected $dataAccess = null;

    public function getDependencies()
    {
        $this->user = $this->getApiUser();
        $this->response = $this->getResponse();
        $this->request = $this->getRequest();

        $dataTransformer = new EmployeeClassRuleDataTransformer();

        $this->dataAccess = new EmployeeClassRuleRepositorySQL($dataTransformer);
    }

    /**
     * @throws \Exception
     */
    public function getEmployeeClassRule()
    {
        $this->getDependencies();

        $getService = new Get();

        $response = $getService->getEmployeeClassRules(
            $this->request,
            $this->response,
            $this->dataAccess
        );

        return $response;
    }

    /**
     * @throws \Exception
     */
    public function getEmployeeClassRuleTypeahead()
    {
        $this->getDependencies();

        $getService = new Get();

        $response = $getService->getEmployeeClassRulesTypeahead(
            $this->request,
            $this->response,
            $this->dataAccess
        );

        return $response;
    }
}
