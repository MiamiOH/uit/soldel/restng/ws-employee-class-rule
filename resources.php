<?php

return [
    'resources' => [
        'employeeClassRule' => [
            MiamiOH\WSEmployeeClassRule\Resources\EmployeeClassRuleResourceProvider::class
        ],
    ]
];
